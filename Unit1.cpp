/*Это оконная программа разработанная на языке С++ в билдере Turbo C++*/
//---------------------------------------------------------------------------
#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "About.h" //окно с некоторыми сведениями о разработчике
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
TForm1 *Form1;
//---------------------------------------------------------------------------
__fastcall TForm1::TForm1(TComponent* Owner)
	: TForm(Owner)
{
	Button1->Enabled = false;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button1Click(TObject *Sender) //Обработка нажатия на кнопку
{
    float r1, r2, r;
    r1 = StrToFloat(Edit1->Text);
    r2 = StrToFloat(Edit2->Text);
    if (RadioGroup1->ItemIndex == 0) {
        r = r1 + r2; //операция сложения
        Label4 ->Caption ="Ответ: " + FloatToStr(r);
    }
    else if(RadioGroup1->ItemIndex == 1){
        r = r1 - r2; //операция вычитания
        Label4 ->Caption = "Ответ: " +FloatToStr(r);
    }
    else if(RadioGroup1->ItemIndex == 2){
        r = r1 * r2; //операция умножения
        Label4 ->Caption = "Ответ: " + FloatToStr(r);
    }
    else if (RadioGroup1->ItemIndex == 3){
        try{
        r = r1/r2; //операция деления
    }
       catch(EInvalidOp &e){
           ShowMessage("Второе число не может быть равно 0!");
           return;
}
    Label4 ->Caption ="Ответ: " + FloatToStr(r);
}
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Button2Click(TObject *Sender) //Обработка нажатия на кнопку выхода
{
    Form1->Close();	
}
//---------------------------------------------------------------------------
void __fastcall TForm1::RadioGroup1Click(TObject *Sender)
{
    if (((Edit1->Text).Length() !=0) && ((Edit2->Text).Length() != 0)) {
        Button1->Enabled = true;
        Form1->Schet->Enabled = true;
}
}
//---------------------------------------------------------------------------

void __fastcall TForm1::Button3Click(TObject *Sender)
{
    AboutBox -> ShowModal(); //открытие окна с информацией при нажании кнопки
}
//---------------------------------------------------------------------------
/*Далее я в меню добавил некоторые кнопки, для которых еще ничего не придумал*/
//---------------------------------------------------------------------------
void __fastcall TForm1::N6Click(TObject *Sender)
{
	MessageBox(0,"Раздела пока нет!","Предупрждение",MB_OK);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::N7Click(TObject *Sender)
{
	MessageBox(0,"Раздела пока нет!","Предупрждение",MB_OK);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::N8Click(TObject *Sender)
{
	MessageBox(0,"Раздела пока нет!","Предупрждение",MB_OK);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::N9Click(TObject *Sender)
{
	MessageBox(0,"Раздела пока нет!","Предупрждение",MB_OK);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::N3Click(TObject *Sender)
{
	MessageBox(0,"Раздела пока нет!","Предупрждение",MB_OK);
}
//---------------------------------------------------------------------------
void __fastcall TForm1::N5Click(TObject *Sender)
{
if (Label4->Caption!="Ответ: ") {

	if (MessageBox(0, "Стереть ответ?", "Подтвердите стирание", MB_YESNO)==IDYES) {
	     Label4->Caption = "Ответ: ";
	}
} ;
}
//---------------------------------------------------------------------------
void __fastcall TForm1::N11Click(TObject *Sender)
{
  AboutBox -> ShowModal();
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit1Change(TObject *Sender)
{
     if ((RadioGroup1->ItemIndex>-1)&&((Edit2->Text).Length()!=0)) {
     Button1->Enabled = true;
     Form1->Schet->Enabled = true;
 }
}
//---------------------------------------------------------------------------
void __fastcall TForm1::Edit2Change(TObject *Sender)
{
    if ((RadioGroup1->ItemIndex>-1)&&((Edit1->Text).Length()!=0)) {

        Button1->Enabled = true;
        Form1->Schet->Enabled = true;
 }
}
//---------------------------------------------------------------------------

void __fastcall TForm1::F11Click(TObject *Sender)
{
   ShellExecute(Handle, "open", "Help.chm", 0, 0, SW_SHOWNORMAL);//добавление .chm хелпера для программы
}
//---------------------------------------------------------------------------

